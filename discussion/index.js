// console.log("Hello World")


//	REPITITION AND CONTROL STRUCTURES
/*
	While Loop
	- a while loop takes in an expression/condition
	- if the condition evaluate to be true, the statements inside the code will executed
	- Syntax:
		while (expression/condition){
			statement
		}
*/

let count = 5;


//	while the value of count is not equal to 0, it will run

while (count !== 0){

	//the current value of count is printed out
	console.log("While: " + count);

	// decrement
	count--;
}s

// Do While Loop
/*
	- A do while loop works a lot like the do while loop but it guarantees that the code will be executed atleast once.
	- Syntax:
		do{
			statement
		} while(expression/condition)
*/

let number = Number(prompt("Give me a number"));

do {
	// the current value of number is printed out
	console.log("Do While: " + number);

	//increases the value of number by 1 after every iteration
	//number = number + !
	number +=1;

	//providing a number of 10 or greater will run the code block once but the loop will stop there
} 
while (number < 10);

// For Loop
/*
	- it is more flexible than while and do while loops.
	- it consist of three parts
		1. initial value - which is the starting value. The count will start at the initial value
		2. condition/expression - determins if the loop will run one more time, while this is true, the loop will run.
		3. final expression - determines how the loop will run (increment/decrement)
	- Syntax:
		for(initial value; expression/condition; finalExpression){
		statement
		}
*/

// This loop starts from 0
// This loops will run as long as the value of count is less than or equal to 20
for (let count = 0; count <= 20; count ++){
	console.log(count);
}

// Characters is string may be counted using the .length property
// Strings are special compared to other data type as it has access to functions and other pieces of information. Other primitive data doesn't have this.

let myString = "jeru";
console.log(myString.length);
//console.log(myString[3]);

//this loops prints the individual letters of the myString variable
for (let x = 0; x < myString.length; x++){
	// the current value of myString is printed out using its index value
	console.log(myString[x]);
}

/*
	- this loop will print out letters of the name individually but will print 3 if the letter is vowel.
	- this loop starts with the value of 0 assigned to i
	- this loop will run as long as the value of i is less than the length of the name
	- this loop increments
*/
let myName = "johndaniel";

for(let i = 0; i < myName.length; i++){
	// if the character of your name is vowel letter, it will display 3 instead of the letter.
	if (myName[i].toLowerCase() == "a" || 
		myName[i].toLowerCase() == "e" || 
		myName[i].toLowerCase() == "i" || 
		myName[i].toLowerCase() == "o" || 
		myName[i].toLowerCase() == "u"){
		console.log(3);
	}
	else {
		console.log(myName[i]);
	}
}

// Continue and Break
/*
	- the continue statement allows the code to go to the next iteration of the loop without finishing the execution of the statements in a code block.
	- the break statement is used to terminate the loop once a match has been found.
*/

for (let count = 0; count <= 20; count++){

	//if the remaindedr is equal to 0
	if (count%2 === 0){
		//tells the code to continue to the next iteration of the loop
		//ignores all the statements located after the continue statement continue;
		continue;
	}
	// the current value if the number is printed out if the remainder is not equal to 0;
	console.log("Continue and Break: " + count);

	if(count > 10){
		// tells the code to terminate/stop the loop even if the condition/expression of the loop defines that it sould execute as long as the value of count is less than or equal to 20
		break;
	}
}

let name = "jakelexter";

for (let i = 0; i < name.length; i++){
	// the current letter is printed out based on its index
	console.log(name[i]);

	if (name[i].toLowerCase() === "a"){
		console.log("Continue to the next iteration");
		continue;
	}

	if (name[i] == "x"){
		break;
	}
}